package com.example.article.service;
import com.example.article.model.Student;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public interface StudentService {

    public List<Student> getAllStudents();
    public Student findStudentByID(int id );
    public void AddStudentMethod(Student student);

    public void deleteData(int id );
    public Student updateStudent(Student student);
}
