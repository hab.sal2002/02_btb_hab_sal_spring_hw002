package com.example.article.service.serviceImp;

import com.example.article.model.Student;
import com.example.article.repository.StudentRepository;
import com.example.article.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StudentServiceImp implements StudentService {
    @Autowired
    StudentRepository studentRepository;

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.getAllStudent();
    }

    @Override
    public Student findStudentByID(int id) {
        return studentRepository.findStudentByID(id);
    }
    @Override
    public void AddStudentMethod(Student student) {
        studentRepository.addNewStudent(student);
    }

    @Override
    public void deleteData(int id) {
        studentRepository.deleteData(id);
    }

    @Override
    public Student updateStudent(Student student) {
        return student;
    }
}
