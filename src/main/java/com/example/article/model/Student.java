package com.example.article.model;


import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Student {
    private int id;
    @NotEmpty(message = "Title cannot be empty..")
    private String title;

    @NotEmpty(message = "Description cannot be empty..")
    private String description;

    private  String profile;

    private   MultipartFile file;

}
