package com.example.article.controller;
import com.example.article.model.Student;
import com.example.article.service.FileStorageService;
import com.example.article.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

@Controller
public class StudentController {

    @Autowired
  @Qualifier("studentServiceImp")
    StudentService studentService;
    // 3 methods : field, setter, constructor

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping
    public String index(Model model){
        model.addAttribute("student", new Student());
        model.addAttribute("students",studentService.getAllStudents());
        return "index";
    }


    @GetMapping("/form-add")
    public String showFormAdd(Model model){
        model.addAttribute("student",new Student());

        return "form-add";
    }
    @PostMapping("/handle-add")
    public String handleAdd(@ModelAttribute @Valid Student student ,BindingResult bindingResult, Errors error){
        if (student.getFile().isEmpty()){
            System.out.println("File is empty "+student.getFile());
            student.setProfile("https://www.sylff.org/wp-content/uploads/2016/04/noImage.jpg");
        }
        else {
            try{
         String filename = "http://localhost:8080/images/"+fileStorageService.saveFile(student.getFile());
          System.out.println("filename: "+filename);
           student.setProfile(filename);
        }catch (IOException ex){
            System.out.println("Error with the image upload "+ex.getMessage());
        }
        }
        System.out.println("Here is result value : "+student);
 if (error.hasErrors()){
     System.out.println("Error");
     return "/form-add";
  }
        System.out.println("Value of student : "+student);
        studentService.AddStudentMethod(student);
      return  "redirect:/";
    }

    @GetMapping("/view-student/{id}")
    public String viewStudent(@PathVariable int id,Model model){
        Student resultStudent = studentService.findStudentByID(id);
        model.addAttribute("student",resultStudent);
        return "form-view";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable int id,@ModelAttribute("student") Student students)
    {
        studentService.deleteData(id);
        return "redirect:/";
    }




    //update
    @GetMapping("/update/edit/{id}")
    public String upDateData(@PathVariable int id, Model model)
    {
        model.addAttribute("student",studentService.findStudentByID(id));
        return "Update-form";
    }
    @PostMapping("/update/{id}")
    public String update(@PathVariable int id, @ModelAttribute("student") Student students, Model model) throws IOException {
        Student existingSubject = studentService.findStudentByID(id);
        try{
            System.out.println("File :  "+ students.getFile().getContentType());
            String filename= "http://localhost:8080/images/"+fileStorageService.saveFile(students.getFile());
            existingSubject.setProfile(filename);
        }catch (IOException e)           {
            System.out.println(e.getMessage());
        }
        existingSubject.setId(id);
        existingSubject.setTitle(students.getTitle());
        existingSubject.setDescription(students.getDescription());
//        existingSubject.setProfile(students.getProfile());
        System.out.println("============>Here is profile linnk of subject.get profile "+ students.getProfile());
        System.out.println("===========New exisitingObjedt Object"+existingSubject);
        System.out.println("Profile of exit "+existingSubject.getProfile() );
//        System.out.println("He file"+students.getFile());
//        System.out.println("File :  "+ students.getProfile());
        studentService.updateStudent(existingSubject);
        return "redirect:/";
    }

}
