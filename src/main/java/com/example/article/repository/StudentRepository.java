package com.example.article.repository;

import com.github.javafaker.Faker;
import com.example.article.model.Student;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepository {

    List<Student> students = new ArrayList<>();
   public StudentRepository(){
        for (int i =0 ; i<5 ; i++){
            Student student = new Student();
            Faker faker = new Faker();
            student.setId(i+1);
            student.setTitle("Spring");
            student.setProfile("https://4.bp.blogspot.com/-9kYSwCDRbms/W-qSUvwnFWI/AAAAAAAAEsE/j4EeFEPQHBc-QpxMV9l3gQAaLAuG2WhTgCLcBGAs/s1600/spring-framework.png");
            student.setDescription("This tutorial covers all Spring framework.");
            students.add(student);
        }
    }

  public   List<Student> getAllStudent(){
        return students;
    }

    public void addNewStudent(Student student){
       student.setId(students.size()+1);
       students.add(student);
    }
    public Student findStudentByID(int id ){
       return students.stream().filter(student -> student.getId()== id).findFirst().orElseThrow();
    }
    public void deleteData(int id){
        for(int i =0; i<students.size();i++)
        {
            if(id==students.get(i).getId())
            {
                students.remove(i);
            }
        }

    }
}
